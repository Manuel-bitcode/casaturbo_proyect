import React, { Fragment, useState } from 'react';
import './Navbar.css';
import { Link } from 'react-router-dom';
import { MenuItems } from './MenuItems';

function Navbar() {
	const [ click, setClick ] = useState(false);

	const handleClick = () => {
		setClick(!click);
	};

	return (
		<Fragment>
			<nav className="navbar">
				<div className="navbar-container">
					<Link to="/" className="navbar-logo">
						<img className="logo-img" src="/img/logo.png" alt="CasaDelTurbo" />
					</Link>
					<div className="menu-icon" onClick={handleClick}>
						<i
							className={

									click ? 'fas fa-times' :
									'fas fa-bars'
							}
						/>
					</div>
					<ul
						className={

								click ? 'nav-menu active' :
								'nav-menu'
						}
					>
						{MenuItems.map((item, index) => {
							return (
								<li key={index} className="nav-item">
									<Link to={item.clink} className="nav-links">
										{item.cName}
									</Link>
								</li>
							);
						})}
					</ul>
				</div>
			</nav>
		</Fragment>
	);
}

export default Navbar;
