export const MenuItems = [
    {
        cName: 'Inicio',
        clink: '/',
    },
    {
        cName: 'Servicios',
        clink: '/servicios',
    },
    {
        cName: 'Nosotros',
        clink: '/nosotros',
    },
    {
        cName: 'Contacto',
        clink: '/contacto',
    }
]