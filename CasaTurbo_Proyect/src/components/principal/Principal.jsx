import React, { Fragment } from 'react';
import './Principal.css';
const Principal = () => {
	return (
		<Fragment>
			<div className="principal-container">
				<div className="web-container">
					<ul className="social-media">
						<li>
							<a href="#" />
							<i className="fab fa-instagram" />
						</li>
						<li>
							<a href="#" />
							<i className="fab fa-facebook-f" />
						</li>
						<li>
							<a href="#" />
							<i className="fab fa-twitter" />
						</li>
					</ul>
					<div className="corner-blue" />
				</div>
				<div className="info-container">
					<h1 className="info-title">Reparacion,venta y mantenimiento</h1>
					<p>Toda clase de turbos, bombas e inyectores, servicio de scanner, balanceo, samblasteo y torno.</p>
					<button>Ver servicios</button>
				</div>
				<div className="img-container">
					<img src="/img/auto-principal.png" alt="car-principal" />
				</div>
			</div>
		</Fragment>
	);
};

export default Principal;
